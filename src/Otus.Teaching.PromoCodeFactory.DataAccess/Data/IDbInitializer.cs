﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
