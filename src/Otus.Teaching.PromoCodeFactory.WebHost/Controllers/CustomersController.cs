﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customersRepository { get; set; }
        private IRepository<PromoCode> _promocodesRepository { get; set; }
        private IRepository<Preference> _preferencesRepository { get; set; }
        private IRepository<CustomerPreference> _customerPreferenceRepository { get; set; }

        public CustomersController(IRepository<Customer> customersRepository,
                                   IRepository<PromoCode> promocodesRepository,
                                   IRepository<Preference> preferencesRepository,
                                   IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customersRepository = customersRepository;
            _promocodesRepository = promocodesRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _preferencesRepository = preferencesRepository;
        }

        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            var responce = customers.Select(customer =>
                new CustomerShortResponse
                {
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    Id = customer.Id,
                });


            return Ok(customers);
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            var customerPreferences = await _customerPreferenceRepository.GetAllAsync();

            var preferencesIds = customerPreferences.Where(preference => preference.CustomerId == id).Select(preference => preference.PreferenceId);

            var preferences = new List<PreferenceResponse>();

            foreach (var preferencesId in preferencesIds)
            {
                var p = await _preferencesRepository.GetByIdAsync(preferencesId);

                preferences.Add(new PreferenceResponse()
                {
                    Id = preferencesId,
                    Name = p.Name,
                });
            }

            var responce = new CustomerResponse
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Id = customer.Id,
                Preferences = preferences,
            };

            return Ok(responce);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            await _customersRepository.AddAsync(
                new Customer()
                {
                    Id = Guid.NewGuid(),
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                });

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;


            foreach (var customerPreferences in customer.CustomerPreferences)
            {
                await _customerPreferenceRepository.DeleteAsync(customerPreferences);
            }

            foreach (var preferenceId in request.PreferenceIds)
            {
                await _customerPreferenceRepository.AddAsync(
                   new CustomerPreference()
                   {
                       CustomerId = customer.Id,
                       PreferenceId = preferenceId,

                   });
            }

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            await _customersRepository.DeleteAsync(customer);

            foreach (var code in customer.PromoCodes)
                await _promocodesRepository.DeleteAsync(code);

            return Ok();
        }
    }
}