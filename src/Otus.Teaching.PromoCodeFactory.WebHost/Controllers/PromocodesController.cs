﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<Customer> _customersRepository { get; set; }
        private IRepository<PromoCode> _promocodesRepository { get; set; }
        private IRepository<Preference> _preferencesRepository { get; set; }
        private IRepository<CustomerPreference> _customerPreferenceRepository { get; set; }

        public PromocodesController(IRepository<Customer> customersRepository,
                                   IRepository<PromoCode> promocodesRepository,
                                   IRepository<Preference> preferencesRepository,
                                   IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customersRepository = customersRepository;
            _promocodesRepository = promocodesRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _preferencesRepository = preferencesRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var codes = await _promocodesRepository.GetAllAsync();

            return Ok(codes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// Из п.4: будем считать, что в данном примере промокод может быть выдан только одному клиенту из базы.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var requestPreferenceId = (await _preferencesRepository.GetWhere(p => p.Name == request.Preference)).FirstOrDefault().Id;

            var customer = (await _customersRepository.GetWhere(c => c.CustomerPreferences.Any(p => p.PreferenceId == requestPreferenceId))).FirstOrDefault();

            var newPromoCode = new PromoCode()
            {
                BeginDate = DateTime.Now,
                EndDate = DateTime.MaxValue,
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                Customer = customer,
                CustomerId = customer.Id,
            };
            
            await _promocodesRepository.AddAsync(newPromoCode);

            return Ok(newPromoCode);
        }
    }
}